package com.example.login.pojos.user;

import com.google.gson.annotations.SerializedName;

public class LogoutResponse {
    @SerializedName("message")
    public String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
