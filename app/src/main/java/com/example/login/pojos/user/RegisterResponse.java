package com.example.login.pojos.user;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("userid")
    public String UserId;

    @SerializedName("name")
    public String Name;

    @SerializedName("email")
    public String Email;

    @SerializedName("password")
    public String Password;

    @SerializedName("confirm_password")
    public String ConfirmPassword;

    public RegisterResponse(String name, String email, String password, String confirmPassword) {
        this.Name = name;
        this.Email = email;
        this.Password = password;
        this.ConfirmPassword = confirmPassword;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        ConfirmPassword = confirmPassword;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}
