package com.example.login.pojos.user;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("email")
    public String Email;

    @SerializedName("password")
    public String Password;

    @SerializedName("token")
    public String Token;

    public LoginResponse(String email, String password) {
        Email = email;
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }
}
