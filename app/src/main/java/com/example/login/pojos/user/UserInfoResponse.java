package com.example.login.pojos.user;
import com.google.gson.annotations.SerializedName;

public class UserInfoResponse {
    @SerializedName("userid")
    public String UserId;

    @SerializedName("name")
    public String Name;

    @SerializedName("email")
    public String Email;

    @SerializedName("token")
    public String Token;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }
}
