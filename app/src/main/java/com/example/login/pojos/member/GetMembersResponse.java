package com.example.login.pojos.member;
import com.google.gson.annotations.SerializedName;

public class GetMembersResponse {
    @SerializedName("surname")
    public String Surname;

    @SerializedName("firstname")
    public String Firstname;

    @SerializedName("address")
    public String Address;

    @SerializedName("zipcode")
    public String Zipcode;

    @SerializedName("telephone")
    public String Telephone;

    @SerializedName("recommendedby")
    public String Recommendedby;

    @SerializedName("joindate")
    public String Joindate;

    @SerializedName("createdby")
    public String Createdby;

    @SerializedName("memid")
    public String Memid;

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getRecommendedby() {
        return Recommendedby;
    }

    public void setRecommendedby(String recommendedby) {
        Recommendedby = recommendedby;
    }

    public String getCreatedby() {
        return Createdby;
    }

    public void setCreatedby(String createdby) {
        Createdby = createdby;
    }

    public String getMemid() {
        return Memid;
    }

    public void setMemid(String memid) {
        Memid = memid;
    }

    public String getJoindate() {
        return Joindate;
    }

    public void setJoindate(String joindate) {
        Joindate = joindate;
    }

    public GetMembersResponse(String surname, String firstname, String address, String zipcode, String telephone) {
        Surname = surname;
        Firstname = firstname;
        Address = address;
        Zipcode = zipcode;
        Telephone = telephone;
    }
}
