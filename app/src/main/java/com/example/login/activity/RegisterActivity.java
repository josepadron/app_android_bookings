package com.example.login.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.login.R;
import com.example.login.interfaces.ApiClient;
import com.example.login.interfaces.UserInterface;
import com.example.login.pojos.user.RegisterResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    TextView txtName, txtEmail, txtPassword, txtConfirmPassword;
    SharedPreferences myprefs;
    UserInterface userInterface;
    private static final String TAG = "RegisterActivity";
    String name, email, password, confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        myprefs = this.getSharedPreferences("token", MODE_PRIVATE);
        userInterface  = ApiClient.getClient().create(UserInterface.class);

        txtName = findViewById(R.id.txtRegisterName);
        txtEmail = findViewById(R.id.txtRegisterEmail);
        txtPassword = findViewById(R.id.txtRegisterPassword);
        txtConfirmPassword = findViewById(R.id.txtRegisterConfirmPassword);
    }

    public void onClickBtnRegister(View v) {
        if (checkValidation()) {
            final RegisterResponse register = new RegisterResponse(name, email, password, confirmPassword);
            Call<RegisterResponse> call1 = userInterface.createUser(register);

            call1.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                    RegisterResponse registerResponse = response.body();

                    Log.i(TAG, "registerResponse --> " + registerResponse);

                    if (response.isSuccessful()) {
                        Log.i(TAG, "getUserId        -->  " + registerResponse.getUserId());
                        Log.i(TAG, "getName        -->  " + registerResponse.getName());
                        Log.i(TAG, "getEmail       -->  " + registerResponse.getEmail());

                        Toast.makeText(RegisterActivity.this, "Register successful", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        finish();
                        startActivity(intent);
                    } else {
                        Toast.makeText(RegisterActivity.this, "registerResponse isn't successful", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                    call.cancel();
                }
            });
        }
    }

    public boolean checkValidation() {
        name = txtName.getText().toString();
        email = txtEmail.getText().toString();
        password = txtPassword.getText().toString();
        confirmPassword = txtConfirmPassword.getText().toString();

        Log.i(TAG, "Check validation begins");
        Log.i(TAG, "name is -> " + email);
        Log.i(TAG, "email is -> " + email);
        Log.i(TAG, "password is -> " + password);
        Log.i(TAG, "confirmPassword is -> " + password);
        Log.i(TAG, "Check validation ends");

        if (name.trim().equals("") || email.trim().equals("") || password.trim().equals("") || confirmPassword.trim().equals("")) {
            Toast.makeText(this, "All fields mustn't be empty", Toast.LENGTH_LONG).show();

            return false;
        } else if (!password.equals(confirmPassword)) {
            Toast.makeText(this, "Passwords not match", Toast.LENGTH_LONG).show();
            txtPassword.setText("");
            txtConfirmPassword.setText("");

            txtPassword.requestFocus();
        }

        return true;
    }
}