package com.example.login.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.login.R;
import com.example.login.interfaces.ApiClient;
import com.example.login.interfaces.UserInterface;
import com.example.login.pojos.user.LoginResponse;

public class LoginActivity extends AppCompatActivity {
    TextView txtEmail, txtPassword;
    String email, password;
    UserInterface userInterface;
    private static final String TAG = "LoginActivity";
    SharedPreferences myprefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myprefs = this.getSharedPreferences("token", MODE_PRIVATE);

        userInterface  = ApiClient.getClient().create(UserInterface.class);

        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
    }

    public void onClickBtnLogin(View v) {
        if (checkValidation()) {
            email = txtEmail.getText().toString();
            password = txtPassword.getText().toString();

            loginRetrofit2Api(email, password);
        }
    }

    public void onClickBtnGoToRegister(View v) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    public boolean checkValidation() {
        email = txtEmail.getText().toString();
        password = txtPassword.getText().toString();

        Log.i(TAG, "Check validation begins");
        Log.i(TAG, "email is -> " + email);
        Log.i(TAG, "password is -> " + password);
        Log.i(TAG, "Check validation ends");

        if (email.trim().equals("")) {
            Toast.makeText(this, "Email no puede estar vacio", Toast.LENGTH_LONG).show();

            return false;
        } else if (password.trim().equals("")) {
            Toast.makeText(this, "Clave no puede estar vacio", Toast.LENGTH_LONG).show();

            return false;
        }

        return true;
    }

    private void loginRetrofit2Api(String email, String password) {
        final LoginResponse login = new LoginResponse(email, password);
        Call<LoginResponse> call1 = userInterface.loginUser(login);

        call1.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                Log.i(TAG, "loginResponse --> " + loginResponse);

                if (response.isSuccessful()) {
                    Log.i(TAG, "getEmail        -->  " + loginResponse.getEmail());
                    Log.i(TAG, "getToken        -->  " + loginResponse.getToken());

                    Toast.makeText(LoginActivity.this, "Welcome " + loginResponse.getEmail(), Toast.LENGTH_SHORT).show();


                    myprefs.edit().putString("email", loginResponse.getEmail()).apply();
                    myprefs.edit().putString("jwt", loginResponse.getToken()).apply();

                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    finish();
                    startActivity(intent);
                } else {
                    if (response.code() == 401) {
                        Toast.makeText(LoginActivity.this, "Invalid email or password", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, "loginResponse isn't successful", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }
}