package com.example.login.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.login.R;
import com.example.login.interfaces.ApiClient;
import com.example.login.interfaces.MemberInterface;
import com.example.login.pojos.member.GetMembersResponse;

import java.io.IOException;

import kotlin.reflect.KFunction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberDetailActivity extends AppCompatActivity {
    MemberInterface memberInterface;
    private static final String TAG = "MemberDetailActivity";
    SharedPreferences myprefs;
    TextView txtMemId, txtFirstName, txtSurName, txtAddress, txtZipcode, txtTelephone, txtRecommendedBy, txtJoinDate, txtCreatedBy;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);

        Bundle b = getIntent().getExtras();
        id = b.getString("id");
        myprefs = this.getSharedPreferences("token", MODE_PRIVATE);

        txtMemId = findViewById(R.id.txtMemberDetailID);
        txtFirstName = findViewById(R.id.txtMemberDetailFirstName);
        txtSurName = findViewById(R.id.txtMemberDetailSurName);
        txtAddress = findViewById(R.id.txtMemberDetailAddress);
        txtZipcode = findViewById(R.id.txtMemberDetailZipcode);
        txtTelephone = findViewById(R.id.txtMemberDetailTelephone);
        txtRecommendedBy = findViewById(R.id.txtMemberDetailRecommendedby);
        txtJoinDate = findViewById(R.id.txtMemberDetailJoinDate);
        txtCreatedBy = findViewById(R.id.txtMemberDetailCreatedby);


        if (!(id == null)) {
            memberInterface = ApiClient.getClient().create(MemberInterface.class);

            Call<GetMembersResponse> call1 = memberInterface.getMemberById("Bearer " + myprefs.getString("jwt", "NULLO"), id);

            call1.enqueue(new Callback<GetMembersResponse>() {
                @Override
                public void onResponse(Call<GetMembersResponse> call, Response<GetMembersResponse> response) {
                    GetMembersResponse getMembersResponse = response.body();

                    Log.i(TAG, "getMembersResponse --> " + getMembersResponse);

                    if (response.isSuccessful()) {
                        txtMemId.setText(getMembersResponse.getMemid());
                        txtFirstName.setText(getMembersResponse.getFirstname());
                        txtSurName.setText(getMembersResponse.getSurname());
                        txtAddress.setText(getMembersResponse.getAddress());
                        txtZipcode.setText(getMembersResponse.getZipcode());
                        txtTelephone.setText(getMembersResponse.getTelephone());

                        if (getMembersResponse.getRecommendedby() == null) {
                            txtRecommendedBy.setText(getMembersResponse.getRecommendedby());
                        } else {
                            txtRecommendedBy.setText("vacio menor");
                        }

                        txtJoinDate.setText(getMembersResponse.getJoindate());
                        txtCreatedBy.setText(getMembersResponse.getCreatedby());
                    } else {
                        Toast.makeText(MemberDetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetMembersResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                    call.cancel();
                }
            });
        }
    }

    public void onClickBtnMemberEdit(View v) {
        GetMembersResponse payload = new GetMembersResponse(txtFirstName.getText().toString().trim(), txtSurName.getText().toString().trim(), txtAddress.getText().toString().trim(), txtZipcode.getText().toString().trim(), txtTelephone.getText().toString().trim());
        Call<GetMembersResponse> call1 = memberInterface.updateMember("Bearer " + myprefs.getString("jwt", "NULLO"), id, payload);

        call1.enqueue(new Callback<GetMembersResponse>() {
            @Override
            public void onResponse(Call<GetMembersResponse> call, Response<GetMembersResponse> response) {
                GetMembersResponse getMembersResponse = response.body();

                Log.i(TAG, "getMembersResponse --> " + getMembersResponse);

                if (response.isSuccessful()) {
                    Toast.makeText(MemberDetailActivity.this, "Member edited correctly", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MemberDetailActivity.this, "Error editing member", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetMembersResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                call.cancel();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    public void onClickBtnMemberDelete(View v) {
        Call<Void> call1 = memberInterface.deleteMember("Bearer " + myprefs.getString("jwt", "NULLO"), id);

        call1.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i(TAG, "deleteResponse --> ");

                if (response.isSuccessful()) {
                    Toast.makeText(MemberDetailActivity.this, "Member deleted correctly", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MemberDetailActivity.this, "Error deleting member", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                call.cancel();
                Log.e(TAG, t.getMessage());
            }
        });
    }
}