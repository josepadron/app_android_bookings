package com.example.login.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.login.R;
import com.example.login.interfaces.ApiClient;
import com.example.login.interfaces.MemberInterface;
import com.example.login.pojos.member.GetMembersResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembersActivity extends AppCompatActivity {
    SharedPreferences myprefs;
    private TableLayout tableLayout;
    MemberInterface memberInterface;
    private static final String TAG = "MembersActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);

        myprefs = this.getSharedPreferences("token", MODE_PRIVATE);

        memberInterface = ApiClient.getClient().create(MemberInterface.class);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        //tableLayout.removeAllViews();

        Call<List<GetMembersResponse>> call1 = memberInterface.getMembers("Bearer " + myprefs.getString("jwt", "NULLO"));

        call1.enqueue(new Callback<List<GetMembersResponse>>() {
            @Override
            public void onResponse(Call<List<GetMembersResponse>> call, Response<List<GetMembersResponse>> response) {
                List<GetMembersResponse> getMembersResponse = response.body();

                Log.i(TAG, "getMembersResponse --> " + getMembersResponse.toString());

                if (getMembersResponse != null) {
                    Toast.makeText(MembersActivity.this, "Members obtained successful", Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < getMembersResponse.size(); i++) {
                        View tableRow = LayoutInflater.from(MembersActivity.this).inflate(R.layout.table_members_item, null, false);

                        TextView memid = (TextView) tableRow.findViewById(R.id.memid);
                        TextView name = (TextView) tableRow.findViewById(R.id.name);
                        TextView address = (TextView) tableRow.findViewById(R.id.address);
                        TextView telephone = (TextView) tableRow.findViewById(R.id.telephone);

                        memid.setText(getMembersResponse.get(i).getMemid());
                        name.setText(String.format("%s %s", getMembersResponse.get(i).getFirstname(), getMembersResponse.get(i).getSurname()));
                        address.setText(getMembersResponse.get(i).getAddress());
                        telephone.setText(getMembersResponse.get(i).getTelephone());


                        tableLayout.addView(tableRow);
                    }
                } else {
                    Toast.makeText(MembersActivity.this, "membersResponse is null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<GetMembersResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "errorOnFailure        -->  " + t.getMessage());
                call.cancel();
            }
        });
    }

    public void onClickBtnMemberEdit(View v) {
        TableRow row = (TableRow) v.getParent();
        TextView view = (TextView) row.getChildAt(0);
        String id = view.getText().toString();


        Intent intent = new Intent(MembersActivity.this, MemberDetailActivity.class);
        intent.putExtra("id", id);

        startActivity(intent);
    }
}