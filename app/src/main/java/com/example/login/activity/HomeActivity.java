package com.example.login.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.login.R;
import com.example.login.interfaces.ApiClient;
import com.example.login.interfaces.UserInterface;
import com.example.login.pojos.user.LogoutResponse;
import com.example.login.pojos.user.UserInfoResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    TextView lblName, lblEmail;
    UserInterface userInterface;
    private static final String TAG = "HomeActivity";
    SharedPreferences myprefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        lblName = findViewById(R.id.lblName);
        lblEmail = findViewById(R.id.lblEmail);

        myprefs = this.getSharedPreferences("token", MODE_PRIVATE);

        userInterface  = ApiClient.getClient().create(UserInterface.class);


        Call<UserInfoResponse> call1 = userInterface.userInfo("Bearer " + myprefs.getString("jwt", "NULLO"));

        call1.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                UserInfoResponse userInfoResponse = response.body();

                Log.i(TAG, "userInfoResponse --> " + userInfoResponse.toString());

                if (userInfoResponse != null) {
                    Log.i(TAG, "getEmail        -->  " + userInfoResponse.getEmail());
                    Log.i(TAG, "getName        -->  " + userInfoResponse.getName());
                    Log.i(TAG, "getUserId        -->  " + userInfoResponse.getUserId());

                    Toast.makeText(HomeActivity.this, "Welcome " + userInfoResponse.getName(), Toast.LENGTH_SHORT).show();

                    lblName.setText(userInfoResponse.getName());
                    lblEmail.setText(userInfoResponse.getEmail());
                } else {
                    Toast.makeText(HomeActivity.this, "userInfoResponse es null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "errorOnFailure        -->  " + t.getMessage());
                call.cancel();
            }
        });
    }

    public void onClickBtnLogout(View v) {
        Call<LogoutResponse> call1 = userInterface.logout("Bearer " + myprefs.getString("jwt", "NULLO"));

        call1.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                LogoutResponse logoutResponse = response.body();

                Log.i(TAG, "logoutResponse --> " + logoutResponse.toString());

                if (response.isSuccessful()) {
                    Log.i(TAG, "getEmail        -->  " + logoutResponse.getMessage());

                    myprefs.edit().clear().apply();

                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    finish();
                    startActivity(intent);
                }

                Toast.makeText(HomeActivity.this, logoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailure called ", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "errorOnFailure        -->  " + t.getMessage());
                call.cancel();
            }
        });
    }


    public void onClickBtnGoToMembers(View v) {
        Intent intent = new Intent(HomeActivity.this, MembersActivity.class);
        startActivity(intent);
    }
}