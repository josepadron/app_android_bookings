package com.example.login.interfaces;

import com.example.login.pojos.member.GetMembersResponse;
import com.example.login.pojos.user.LoginResponse;
import com.example.login.pojos.user.LogoutResponse;
import com.example.login.pojos.user.RegisterResponse;
import com.example.login.pojos.user.UserInfoResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MemberInterface {
    // List of members (plural)
    @GET("members")
    Call<List<GetMembersResponse>> getMembers(@Header("Authorization") String jwt);

    // One member (singular)
    @GET("members/{member}")
    Call<GetMembersResponse> getMemberById(@Header("Authorization") String jwt, @Path("member") String memid);

    // One member (singular)
    @PUT("members/{member}")
    Call<GetMembersResponse> updateMember(@Header("Authorization") String jwt, @Path("member") String memid, @Body GetMembersResponse memberData);

    // One member (singular)
    @DELETE("members/{member}")
    Call<Void> deleteMember(@Header("Authorization") String jwt, @Path("member") String memid);
}