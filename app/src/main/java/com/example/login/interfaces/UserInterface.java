package com.example.login.interfaces;

import com.example.login.pojos.user.LoginResponse;
import com.example.login.pojos.user.LogoutResponse;
import com.example.login.pojos.user.RegisterResponse;
import com.example.login.pojos.user.UserInfoResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserInterface {
    @GET("user-info")
    Call<UserInfoResponse> userInfo(@Header("Authorization") String jwt);

    @GET("logout")
    Call<LogoutResponse> logout(@Header("Authorization") String jwt);

    @POST("login")
    Call<LoginResponse> loginUser(@Body LoginResponse login);

    @POST("register")
    Call<RegisterResponse> createUser(@Body RegisterResponse register);
}